<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

 

 


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>addPetPage</title>
<style>
.center {
    margin: 50px 300px;
}

 

.right {
    position: absolute;
    right: 325px;
    color: white;
}

 

table {
    border-spacing: 0 10px;
}

 

a {
    color: white;
    text-decoration: none;
}

 

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

 

li {
    float: left;
}

 

li {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

 

li a:hover {
    background-color: #111;
}

 

.container {
    display: table;
    width: 100%
}

 

label {
    display: table-cell;
    width: 1px;
    white-space: nowrap
}

 

span {
    display: table-cell;
    padding: 0 0 0 5px
}

 

input {
    width: 100%
}

 

#test1 {
    margin-left: 11px;
    width: 95%
}

 

nav {
    background: #B2BEB5;
    border: 1px;
    color: white;
    padding: 3px;
    padding-left: 4px;
    width: 82%;
    margin-left:100px;
}


.pet{
margin: 60px 500px;
}



.field{

margin:2%;
margin-left:40%;
}
.main{
margin-left:20%;
}
button{
background: #00BFFF;
color:white;
border-style:none;
border-radius:.25rem;
padding: .375rem .75rem;
}

 

</style>
</head>

 


<body>
    <div class="center">

 

 

        <ul>
            <li>Pet Shop</li>
            <li><a href="home">Home</a></li>
            <li><a href="addPetPage">Add Pet</a></li>
            <li><a href="myPetsPage">My Pet</a></li>
            <li><a href="logout" class="right">Logout</a></li>
        </ul>
    </div>
    <div class="pet">
    <nav>Pet Information</nav>
<br>
<form:form method="post" action="savePet" modelAttribute="pet">
<div class="main" >

 

 Name <input type="text" path="petName"><br>
 <form:errors path="petName" cssStyle="color: #ff0000"></form:errors>
<br>
Age <input type="number" path="petAge"><br>
<form:errors path="petAge" cssStyle="color: #ff0000"></form:errors>
<br>
Place <input type="text" path="petPlace"><br>
<form:errors path="petPlace" cssStyle="color: #ff0000"></form:errors>
<br>

 

 </div>
<div class ="field">
<button>Add Pet</button>
<button>Cancel</button>
</div>

 

</form:form>
</div>
</body>
</html>