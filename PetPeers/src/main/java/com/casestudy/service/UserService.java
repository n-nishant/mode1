package com.casestudy.service;

import com.casestudy.model.User;

public interface UserService {

	public abstract User saveUser(User user);
	
	public abstract User authenticateUser(String userName,String password);

}
