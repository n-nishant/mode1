package com.casestudy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.ModelAttribute;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.PetService;
import com.casestudy.service.UserService;
import com.casestudy.validator.LoginValidator;
import com.casestudy.validator.PetValidator;
import com.casestudy.validator.UserValidator;

@Controller
public class MainController {

	@Autowired

	private User user;

	@Autowired
	private Pet pet;

	@Autowired
	private UserService userService;

	@Autowired
	private PetService petService;

	@Autowired
	private UserValidator userValidator;

	@Autowired
	private LoginValidator loginValidator;

	@Autowired
	private PetValidator petValidator;

	@RequestMapping(value = "/")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		modelAndView.addObject("userModel", user);
		return modelAndView;
	}

	@RequestMapping(value = "/login")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("loginPage");
		modelAndView.addObject("loginModel", user);
		return modelAndView;
	}

	@RequestMapping(value = "/register")
	public ModelAndView register(@ModelAttribute("userModel") User user) {
		ModelAndView modelAndView = new ModelAndView("registrationPage");

		return modelAndView;
	}

	@RequestMapping(value = "saveUser", method = RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute("userModel") User user, BindingResult result, Model map) {
		userValidator.validate(user, result);
		userValidator.validatePassword(user, result);
		// userValidator.validateUser(user, result);
		ModelAndView modelAndView = null;
		if (result.hasErrors()) {
			modelAndView = new ModelAndView("registrationPage");
		} else {
			userService.saveUser(user);
			User user1 = new User();
			map.addAttribute("loginModel", user1);
			modelAndView = new ModelAndView("loginPage");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/authenticateUser")
    public ModelAndView authenticateUser(HttpServletRequest request, @ModelAttribute("loginModel") User user,
            BindingResult result) {
        ModelAndView modelAndView = null;
        loginValidator.validate(user, result);
        loginValidator.validateUsernameAndPassword(user, result);
        if (result.hasErrors()) {
            modelAndView = new ModelAndView("login");
        } else {
            modelAndView = new ModelAndView("redirect:/home");

 

        }
        return modelAndView;
    }

	@RequestMapping(value = "/logout")
	public ModelAndView logout(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("loginPage");
		modelAndView.addObject("loginModel", user);
		return modelAndView;
	}

	@RequestMapping(value = "/home")

    public ModelAndView home() {

    List<Pet> list = petService.getAllPets();

    ModelAndView modelAndView = new ModelAndView("homePage");

    modelAndView.addObject("pList", list);

    return modelAndView;

    }

	@RequestMapping(value = "/addPetPage")
	public ModelAndView addPet() {
		ModelAndView modelAndView = new ModelAndView("addPetPage");
		modelAndView.addObject("pet", pet);

		return modelAndView;
	}
	@RequestMapping(value = "/savePet")
    public ModelAndView savePet(@ModelAttribute("pet") Pet pet, BindingResult result) {
        petValidator.validate(pet, result);
        petValidator.validatePetAge(pet, result);
        ModelAndView modelAndView = null;
        if (result.hasErrors()) {
            modelAndView = new ModelAndView("addPetPage");
        } else {
            petService.savePet(pet);
            modelAndView = new ModelAndView("redirect:/home");
        }
        return modelAndView;
    }

	@RequestMapping(value = "/myPetsPage")
	public ModelAndView myPets() {
		ModelAndView modelAndView = new ModelAndView("myPetsPage");

		return modelAndView;
	}

}
