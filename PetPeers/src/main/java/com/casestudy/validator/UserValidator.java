package com.casestudy.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.User;


public class UserValidator implements Validator {



	@Override
	public boolean supports(Class<?> arg0) {
		return User.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPassword", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "required");
	}

	public boolean validatePassword(User user, Errors errors) {

		if (!user.getUserPassword().equals(user.getConfirmPassword())) {
			errors.rejectValue("confirmPassword", "valid.passwordConfirm");
			return true;
		}
		return false;
	}
	

}
