package com.casestudy.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.Pet;


public class PetValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {

		return Pet.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petName", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petAge", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petPlace", "required");

	}

	
	  public void validatePetAge(Pet pet, Errors errors) {
	  
	  if (pet.getPetAge() < 0 || pet.getPetAge() > 99) {
	  
	  errors.rejectValue("petAge", "invalid.age"); }
	  
	  }
	 

}
